# frozen_string_literal: true
# author: sircapsalot

require 'thor'
require 'tty-prompt'
require 'tty-table'
require 'json'

class Euper < Thor
  MAP = {
    # EUP => Ped Customization
    'Mask': 'Mask / Facial Hair',
    'UpperSkin': 'Hands / Upper Body',
    'Pants': 'Legs / Pants',
    'Parachute': 'Bags / Parachutes',
    'Shoes': 'Shoes',
    'Accessories': 'Neck / Scarfs',
    'UnderCoat': 'Shirt / Accessory',
    'Armor': 'Body Armor / Accessory 2',
    'Decal': 'Badges / Logos',
    'Top': 'Shirt Overlay / Jackets',
    'Hat': 'Hats / Helmets',
    'Glasses': 'Glasses',
    'Ear': 'Misc',
    'Watch': 'Watches',
  }

  OFFSETS = {
    'Hat': -1,
    'Glasses': -1,
    'Watch': -1
  }

  desc 'create CATEGORY NAME', 'create an EUP in category CATEGORY named NAME'
  def create(category, name)
    eup = {
      'Name': name,
      'Category': "EUP",
      'Category2': category
    }

    default = 0
    prompt = TTY::Prompt.new

    prompt.say("EUP #{category} #{name}", color: :blue)

    gender = prompt.select('Is this for a Male of Female?', %w(Male Female))

    eup['Gender'] = gender

    prompt.yes?('Are these numbers from Ped Customization?').tap do |ped_customization|
      default = 1 if ped_customization
    end


    MAP.each do |e, attribute|
      question = default.zero? ? e : attribute
      number = prompt.ask("#{question}#:", default: default) do |q|
        q.required(true)
        q.validate(/\A\d+\Z/, 'must be a number')
        q.convert(:integer)
      end

      texture = prompt.ask("#{question} Texture#:", default: default) do |q|
        q.required(true)
        q.validate(/\A\d+\Z/, 'must be a number')
        q.convert(:integer)
      end

      unless default.zero?
        number += OFFSETS[e] if OFFSETS.include?(e)
      end

      eup[e] = "#{number}:#{texture}"
    end

    puts '-' * 12

    File.open("dist/#{category}.json", 'a+') do |file|
      file << JSON.pretty_generate(eup) << "\n"
    end
  end

  desc 'list', 'list the EUP <-> Ped Customization table'
  def list
    puts TTY::Table.new(['Ped Customization', 'EUP'],
      MAP.map { |k, v| [v, k] }
    ).render(:unicode, alignments: [:center, :center])
  end
end
