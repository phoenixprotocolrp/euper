# EUPer

This is a script written in Ruby where you can easily convert
Player Appearance <=> MP Ped numbers with offsets.

With this tool, you can also create JSON files to manage EUPs.

## Example

```json
{
  "Name": "Formal",
  "Gender": "Male",
  "Category": "EUP",
  "Category2": "SAFR",
  "Hat": "0:0",
  "Glasses": "0:0",
  "Ear": "0:0",
  "Watch": "0:0",
  "Mask": "0:0",
  "Top": "0:0",
  "UpperSkin": "0:0",
  "Decal": "0:0",
  "UnderCoat": "0:0",
  "Pants": "0:0",
  "Shoes": "0:0",
  "Accessories": "0:0",
  "Armor": "0:0",
  "Parachute": "0:0"
},
{
  "Name": "Formal",
  "Gender": "Female",
  "Category": "EUP",
  "Category2": "SAFR"
}
```
